const express = require('express')
const app = express()
const path = require('path')
const exphbs = require('express-handlebars')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const upload = require('express-fileupload')
const session = require('express-session')
const flash = require('connect-flash')
const {mongodbURL} = require('./config/database')
const passport = require('passport');


//======== file uploading ==================
app.use(upload()) // this line always declare before body parser

//body parser
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))


mongoose.connect(mongodbURL, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex:true }).then(db => {
    console.log('database connected')
}).catch(err => console.log(`error is =====  ${err}`))


app.use(express.static(path.join(__dirname, 'public')))

//======== Setup View Engine  =============
const { select, dateGenerator, paginate } = require('./helpers/handlebars-helpers')
app.engine('handlebars', exphbs({ defaultLayout: 'home', helpers: { select: select, dateGenerator: dateGenerator, paginate:paginate} }))
app.set('view engine', 'handlebars')

//===== method override
app.use(methodOverride('_method'))
//=========== session and flash message =========
app.use(session({
    secret: "43456300",
    resave: true,
    saveUninitialized: true,
    cookie: { maxAge: 1800000 }
}))


app.use(flash())
//============ passport initialize ================
app.use(passport.initialize());   //aita flash er pore use korte hoy
app.use(passport.session());

app.use((req, res, next) => {
    res.locals.uuser = req.user || null 
    res.locals.successMessage = req.flash('successMessage')
    res.locals.errorMessage = req.flash('errorMessage')
    res.locals.error = req.flash('error')
    next()
})


//========= load routes ====================
const home = require('./routes/home/index')
const user = require('./routes/home/user')
const admin = require('./routes/admin/index')
//const adminlogin = require('./routes/admin/login')
const userposts = require('./routes/admin/posts')
const category = require('./routes/admin/category')
const comments = require('./routes/admin/comments')
//========= use routes  =====================
app.use('/', home)
app.use('/user', user)
app.use('/admin', admin)
app.use('/user/posts', userposts)
app.use('/admin/category', category)
app.use('/admin/comments', comments)
//app.use('/admin', adminlogin)


let port = 4040 || process.env.port
app.listen(port, () => console.log(`listening on port ${port}`))
