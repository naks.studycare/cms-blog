const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema = new Schema({

    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    passwordConfirm: {
        type: String
    },
    profilepicture:{
        type: String
    },
    coverphoto:{
        type : String
    },
    facebook:{
        type: String
    },
    github:{
        type: String
    },
    livesin:{
        type: String,
        required: true
    }
})

module.exports = mongoose.model('users', UserSchema)