const mongoose = require('mongoose')
const Schema = mongoose.Schema

const mindsSchema = new Schema({
    user :{
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    body:{
        type: String
    },
    date:{
        type: Date, 
        default: Date.now()
    },
    file:{
        type: String
    }
})

module.exports = mongoose.model('minds',mindsSchema)