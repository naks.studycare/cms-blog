module.exports = {
    isEmpty : function(Obj){
        for(let key in Obj){
            if(Obj.hasOwnProperty(key)){
                return false
            }
        }   
        return true
    }
}