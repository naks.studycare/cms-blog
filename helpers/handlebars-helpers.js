const moment = require('moment');


module.exports = {

    select: function (selected, options) {
        return options.fn(this).replace(new RegExp('value=\"' + selected + '\"'), '$&selected="selected" ')
    },
    dateGenerator: function (date, format) {
        return moment(date).format(format);
    },


    paginate: function (option) {
        let output = ''

        if (option.hash.current === 1) {
            output += `<li class='page-item disabled' ><a class='page-link'>First</a></li>`
        }
        else {
            output += `<li class='page-item' ><a href='?page=1' class='page-link'>First</a></li>`
        }


        let i = (Number(option.hash.current) > 5 ? Number(option.hash.current) - 4 : 1)
        if (i != 1) {
            output += `<li class='page-item disabled' ><a class='page-link'>...</a></li>`

        }

        for (; i <= (Number(option.hash.current) + 4) && i <= Number(option.hash.pages); i++) {
            if (i === option.hash.current) {
                output += `<li class='page-item active' ><a class='page-link'>${i}</a></li>`
            }

            else {
                output += `<li class='page-item ' ><a href='?page=${i}' class='page-link'>${i}</a></li>`
            }

            if (i === Number(option.hash.current)+4 && i < option.hash.pages) {
                output += `<li class='page-item disabled' ><a class='page-link'>...</a></li>`

            }
        }

        if (option.hash.current === option.hash.pages) {
            output += `<li class='page-item disabled' ><a class='page-link'>Last</a></li>`

        }
        else {
            output += `<li class='page-item' ><a href='?page=${option.hash.pages}' class='page-link'>Last</a></li>`

        }

        return output

    }
}