const express = require('express')
const router = express.Router()
const post = require('../../models/posts')
const Category = require('../../models/category')
const user = require('../../models/user')
const passport = require('passport')
const Minds = require('../../models/minds')
const { isEmpty } = require('../../helpers/upload-helper')
const fs = require('fs');
const { userAuthenticated} = require('../../helpers/userAuthenticated')

router.all('/*',userAuthenticated, (req, res, next) => {
    req.app.locals.layout = "home"
    next()
})


router.get('/myprofile/:id', (req, res) => {
    user.find({_id:req.params.id})
    .then(useR=>{
        Minds.find({user:req.params.id}).sort({date:-1})
        .populate('user')
        .then(mindsaved=>{
            res.render('user/myprofile',{useR:useR,mindsaved:mindsaved})
        })
    })
})


router.post('/myprofile/:id', (req, res) => {
    const ID = req.user.id

    let mindpic = ""
    if (!isEmpty(req.files)) {
        //==========file send to uploads folder
        let mindpicture = req.files.mindfile
        mindpic = Date.now() + '-' + mindpicture.name
        mindpicture.mv('./public/uploads/user/mind/' + mindpic, (err) => {
            if (err) throw err
        })
    }
   const newMind = new Minds({
       user: req.user.id,
       file: mindpic,
       body:req.body.body
   })

   newMind.save()
   .then(savedmind=>{
        res.redirect('/user/myprofile/'+ID);
   })
})

router.get('/allpost', (req, res) => {
    post.find({user: req.user.id}).sort({date:-1}).populate('category').populate('user')
    .then(posts => {   // ekhane category shema theke object pabar jonno populate use kora hoye se ja name and id return kore
        res.render('user/mypost', { posts: posts})
    })
})

router.get('/draft', (req, res) => {
    post.find({$and:[{status:"draft"},{user:req.user.id}]}).sort({date:-1}).populate('category').populate('user')
    .then(posts => {   // ekhane category shema theke object pabar jonno populate use kora hoye se ja name and id return kore
        res.render('user/mypost', { posts: posts})
    })
})

router.get('/allcomments', (req, res) => {
    post.find({user: req.user.id}).sort({date:-1}).populate('comments').populate('user')
    .then(posts => {   // ekhane category shema theke object pabar jonno populate use kora hoye se ja name and id return kore
        res.render('admin/comment/index',{posts:posts});
    })
})




module.exports = router