const express = require('express')
const router = express.Router()
const bcrypt = require('bcryptjs')
const post = require('../../models/posts')
const Category = require('../../models/category')
const user = require('../../models/user')
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const { isEmpty } = require('../../helpers/upload-helper')
const fs = require('fs');
const nodemailer = require('nodemailer')
const htmlToText = require('html-to-text');
const hidetext = require('hide-text')

router.all('/*', (req, res, next) => {
    req.app.locals.layout = "home"
    next()
})

//====================HOME==========================
router.get('/', (req, res) => {
    const perPage = 10
    const page = req.query.page || 1

    post.find({ status: "public" }).sort({ date: -1 }).populate('user')
        .skip((perPage * page) - perPage)
        .limit(perPage)
        .then(posts => {

            posts.forEach(function (element) {

                const text = htmlToText.fromString(element.body, {
                    wordwrap: 80,
                });

                if (text.length > 300) {
                    let result = hidetext(text, { placeholder: '.', showLeft: 300, showRight: 0, trim: 3 })
                    element.body = result
                }
                else {
                    element.body
                }
            })

            post.count({ status: "public" }).then(postCount => {
                Category.find({}).then(category => {
                    res.render('home/index', {
                        posts: posts,
                        category: category,
                        current: parseInt(page),
                        pages: Math.ceil(postCount / perPage)
                    })
                })
            })

        })
})


//================ portfolio =============================
router.get('/admin-portfolio', (req, res) => {
    req.app.locals.layout = ""
    res.render('layouts/nav/portfolio');
});

//================ contact ========================

router.get('/contact', (req, res) => {
    res.render('layouts/main');
});

router.post('/contact/send', (req, res) => {
    const output = `
      <p>You have a new contact request</p>
      <h3>Contact Details</h3>
      <ul style="color:red">  
        <li>Name: ${req.body.name}</li>
        <li>City: ${req.body.company}</li>
        <li>Email: ${req.body.email}</li>
        <li>Phone: ${req.body.phone}</li>
      </ul>
      <h3>Message</h3>
      <p>${req.body.message}</p>
    `;

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: 'theafterlife018@gmail.com', // generated ethereal user
            pass: '*************'  // generated ethereal password
        },
        tls: {
            rejectUnauthorized: false
        }
    });

    // setup email data with unicode symbols
    let mailOptions = {
        from: 'Durjoy', //req.body.email, // sender address   <theafterlife018@gmail.com>
        to: 'theafterlife018@gmail.com,naks.studycare@gmail.com', // list of receivers
        subject: 'Blog contact', // Subject line
        text: '', // plain text body
        html:output,
        amp:`<!doctype html>
        <html ⚡4email>
          <head>
            <meta charset="utf-8">
            <style amp4email-boilerplate>body{visibility:visible}</style>
            <script async src="https://cdn.ampproject.org/v0.js"></script>
            <script async custom-element="amp-anim" src="https://cdn.ampproject.org/v0/amp-anim-0.1.js"></script>
            <link rel="canonical" href="https://amp.dev/documentation/examples/introduction/hello_world/index.html">
            <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">

            </head>
          <body>
            <p>Image: <amp-img src="https://cldup.com/P0b1bUmEet.png" width="16" height="16"/></p>
            <p>GIF (requires "amp-anim" script in header):<br/>
              <amp-anim src="https://cldup.com/D72zpdwI-i.gif" width="500" height="350"/></p>
          </body>
        </html>`// html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

        res.render('layouts/main', { msg: 'Email has been sent' });
    });
});



//=======================================================================
//====================HOME  Category wise post view ==========================
router.get('/category/:id', (req, res) => {
    const perPage = 10
    const page = req.query.page || 1

    post.find({ category: req.params.id }).sort({ date: -1 }).populate('user')
        .skip((perPage * page) - perPage)
        .limit(perPage)
        .then(posts => {
            post.count({ category: req.params.id }).then(postCount => {

                Category.find({}).then(category => {

                    res.render('home/index', {
                        posts: posts,
                        category: category,
                        current: parseInt(page),
                        pages: Math.ceil(postCount / perPage)
                    })
                })
            })
        })
})

//===================ABOUT==================================
router.get('/about', (req, res) => {
    res.render('home/about')
})

//===================User LOGIN==================================
router.get('/login', (req, res) => {
    res.render('home/login')
})



//===================LOGIN POST==================================

passport.use(new LocalStrategy({ usernameField: 'email' }, (email, password, done) => {

    user.findOne({ email: email }).then(User => {
        if (!User) return done(null, false, { message: 'User not found' })

        bcrypt.compare(password, User.password, (err, checked) => {
            if (err) return err

            if (checked) {
                return done(null, User)
            }
            else {
                return done(null, false, { message: 'Incorrect Password' })
            }
        })
    })
}))


passport.serializeUser(function (User, done) {
    done(null, User.id);
});

passport.deserializeUser(function (id, done) {
    user.findById(id, function (err, User) {
        done(err, User);
    });
});


router.post('/login', (req, res, next) => {
    if (req.body.email == "admin@admin.com" && req.body.password == "admin") {
        passport.authenticate('local', {
            successRedirect: '/admin',
            failureRedirect: '/login',
            failureFlash: true
        })(req, res, next)
    }
    else {
        passport.authenticate('local', {
            successRedirect: '/',
            failureRedirect: '/login',
            failureFlash: true
        })(req, res, next)
    }


})


//===========LOG OUT ==========================
router.get('/logout', (req, res) => {
    req.logOut()
    res.redirect('/login');
});

//===================REGISTER GET============================
router.get('/register', (req, res) => {
    res.render('home/register')
})

//===================REGISTER POST===========================
router.post('/register', (req, res) => {
    let errors = []
    if (!req.body.firstName) {
        errors.push({ message: "Please enter your first name" })
    }
    if (!req.body.lastName) {
        errors.push({ message: "Please enter your lastName" })
    }
    if (!req.body.email) {
        errors.push({ message: "Please enter your email" })
    }
    if (!req.body.password) {
        errors.push({ message: "Please enter a password" })
    }
    if (!req.body.firstName) {
        errors.push({ message: "Please enter repeated password" })
    }
    if (req.body.password != req.body.passwordConfirm) {
        errors.push({ message: "Password dosn't match" })
    }
    if (errors.length > 0) {
        res.render('home/register', {
            errors: errors,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            livesin: req.body.livesin,
            facebook: req.body.facebook,
            github: req.body.github
        });
    }
    else {

        user.findOne({ email: req.body.email }).then(User => {
            if (User) {
                req.flash('errorMessage', '<<Email already exists. Please try with another one.>>')
                res.redirect('/register');
            }

            else {

                let ppname = "pppp.jpg"
                let covername = "cover.jpg"
                if (!isEmpty(req.files)) {
                    //==========file send to uploads folder
                    let profilepic = req.files.profilepicture
                    let coverpic = req.files.coverphoto
                    ppname = Date.now() + '-' + profilepic.name
                    covername = Date.now() + '-' + coverpic.name
                    profilepic.mv('./public/uploads/user/' + ppname, (err) => {
                        if (err) throw err
                    })
                    coverpic.mv('./public/uploads/user/' + covername, (err) => {
                        if (err) throw err
                    })
                }

                const newUser = new user({
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    email: req.body.email,
                    password: req.body.password,
                    profilepicture: ppname,
                    coverphoto: covername,
                    livesin: req.body.livesin,
                    facebook: req.body.facebook,
                    github: req.body.github
                    //passwordConfirm: req.body.passwordConfirm
                })

                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(newUser.password, salt, (err, hash) => {
                        newUser.password = hash
                        newUser.save().then(datasaved => {
                            req.flash('successMessage', `<< Registration Successful. Please login >>`)
                            res.redirect('/login')
                        })
                    })
                })
            }
        })
    }
})


//======================SINGLE POST VIEW========================
router.get('/post/:id', (req, res) => {
    post.findOne({ _id: req.params.id })
        .populate({ path: 'comments', match: { approveComment: true }, populate: { path: 'user', model: 'users' } })
        .populate('user')
        .then(post => {
            Category.find({}).then(category => {

                res.render('home/post', { post: post, category: category })
            })
        })
})




module.exports = router