const express = require('express')
const router = express.Router()
const category = require('../../models/category')
const {userAuthenticated} = require('../../helpers/userAuthenticated')

// admin pannel er jonno alada html veiw tar jonno amra akhane
//default layout k overwrite koresi
router.all('/*', (req, res, next) => {
    req.app.locals.layout = 'admin'
    next()
})

router.get('/', (req, res) => {
    category.find({}).then(category=>{
        res.render('admin/category/index',{category:category})
    })
   
})


router.post('/create', (req, res) => {  

    const newCategory = new category({
        name: req.body.category
    })
    
    newCategory.save().then(saved=>{
        req.flash('successMessage', `<< Category created succesfully >>`)
         res.redirect('/admin/category')
    })
})

router.get('/edit/:id', (req, res) => {
    category.findOne({_id:req.params.id}).then(post=>{
   res.render('admin/category/edit',{post:post})
      
    })
});

router.put('/edit/:id', (req, res) => {
    category.findOne({_id:req.params.id}).then(post=>{
        post.name = req.body.category
        post.save().then(saved=>{
            req.flash('successMessage', `<< Category Edited succesfully >>`)
            res.redirect('/admin/category')
        })
    })
});

router.delete('/:id', (req, res) => {
    category.deleteOne({_id:req.params.id})
    .then(result => {
       req.flash('successMessage', `<< Category deleted succesfully >>`)
        res.redirect('/admin/category') 
    })
});



module.exports = router