const express = require('express')
const faker = require('faker')
const router = express.Router()
const Post = require('../../models/posts')
const Comment = require('../../models/comments')
const Category = require('../../models/category')
const User = require('../../models/user')
const Mind = require('../../models/minds')
const { adminAuthenticated} = require('../../helpers/userAuthenticated')
const { isEmpty } = require('../../helpers/upload-helper')
const fs = require('fs');

// admin pannel er jonno alada html veiw tar jonno amra akhane
//default layout k overwrite koresi
router.all('/*', (req, res, next) => {
    req.app.locals.layout = 'admin'
    next()
})

router.get('/', (req, res) => {

    res.render('admin/index')
})

//============================================
//============= all post for admin=============

router.get('/posts/allpost', (req, res) => {
    Post.find({}).sort({date:-1}).populate('category')
    .then(posts => {   // ekhane category shema theke object pabar jonno populate use kora hoye se ja name and id return kore
        res.render('admin/posts/admin-post', { posts: posts})
    })
})

//===============================================
//=================All USER =====================

router.get('/alluser', (req, res) => {
    User.find({}).sort({firstName:1})
    .then(user=>{
        res.render('admin/posts/alluser',{user:user});
    })

});


//==================================================================
//======================= Delete a USER ============================

router.delete('/user/myprofile/:id', (req, res) => {
    User.findOne({ _id: req.params.id })
    .then(result => {
        fs.unlink('./public/uploads/user/'+result.profilepicture,(err)=>{  //for picture deleting from uploads folder
            fs.unlink('./public/uploads/user/'+result.coverphoto,err=>{
//==================== for comment deleting=========================  
            result.remove().then(removeduser=>{
                req.flash('successMessage', `<< User deleted succesfully >>`)
                res.redirect('/admin/alluser')
            })
        })
        })
    })
});


//================================================
//================DashBoard=======================

router.get('/dashboard', (req, res) => {

    Post.count({}).then(postCounted => {
        Comment.count({}).then(commentCounted => {
            Category.count({}).then(categoryCounted => {
                User.count({}).then(userCounted => {
                    res.render('admin/dashboard', { postCounted: postCounted, commentCounted:commentCounted,categoryCounted:categoryCounted,userCounted:userCounted })
                })
            })
        })
    })
})

//======================================================
//======== route for fake data creation ================
router.post('/fake-post', (req, res) => {
    for (let number = 0; number < req.body.number; number++) {
        let post = new Post()
        post.title = faker.name.title()
        post.status = 'public'
        post.allowComments = faker.random.boolean()
        post.body = faker.lorem.paragraphs()
        post.file = '1572462895243-2019-10-26-19-18-15.jpg'

        post.save()
    }

    res.redirect('/admin/posts')
})


//==================================================================
//====================== For Scrolling text ========================

router.get('/scrolling-text', (req, res) => {
        res.render('admin/scrolling-text');
});


//============ for getting post writting page==========================
//====================================================================== 
router.get('/posts/create', (req, res) => {
    Category.find({}).then(category => {
       
        res.render('admin/posts/admincreate', { category: category })
    })
})


//=========== all information for post and saved to database============
//======================================================================
router.post('/posts/create', (req, res) => {

    let errors = []

    if (!req.body.title) {
        errors.push({ message: 'Please give a title' })
    }
    if (!req.body.body) {
        errors.push({ message: 'This field can not be empty' })
    }

    if (errors.length > 0) {
        res.render('admin/posts/create', {
            errors: errors
        })
    }
    else {
        let filename = ""
        if (!isEmpty(req.files)) {
            //==========file send to uploads folder
            let file = req.files.file
            filename = Date.now() + '-' + file.name
            file.mv('./public/uploads/' + filename, (err) => {
                if (err) throw err
            })
        }

        //================================================
        let allowComments = true
        if (req.body.allowComments) {
            allowComments = true
        }
        else {
            allowComments = false
        }


        const newPost = new Post({
            title: req.body.title,
            status: req.body.status,
            allowComments: allowComments,
            body: req.body.body,
            file: filename,
            category: req.body.category,
           // user : ""
           
        })

        newPost.save().then(datasaved => {
            req.flash('successMessage', `<< ${datasaved.title} saved succesfully >>`)
            res.redirect('/admin/posts/allpost')
            console.log(datasaved)

        }).catch(err => console.log('Error!!! post save failed'))
    }

})


// getting page for editing 
router.get('/posts/edit/:id', (req, res) => {
    Post.findOne({_id:req.params.id}).then(post => {
        Category.find({}).then(category => {
            res.render('admin/posts/adminedit', { post: post, category: category })
        })

    })
})

//for posting edited posts 
router.put('/posts/edit/:id', (req, res) => {

    Post.findOne({_id: req.params.id}).then(Post => {

        if (req.body.allowComments) {
            allowComments = true
        }
        else {
            allowComments = false
        }

        Post.title = req.body.title
        Post.status = req.body.status
        Post.allowComments = allowComments
        Post.body = req.body.body
        Post.category = req.body.category
       // Post.user = ""
       

        if (!isEmpty(req.files)) {
            //==========file send to uploads folder
            let file = req.files.file
            filename = Date.now() + '-' + file.name
            Post.file = filename
            file.mv('./public/uploads/' + filename, (err) => {
                if (err) throw err
            })
        }


        Post.save().then(datasaved => {
            req.flash('successMessage', `<< ${datasaved.title} edited succesfully >>`)
            res.redirect('/admin/posts/allpost')
            console.log(datasaved)
        }).catch(err => console.log('Error!!! post save failed'))
    })
})


// for deleting post 
router.delete('/posts/:id', (req, res) => {
    Post.findOne({ _id: req.params.id })
        .populate('comments')
        .then(result => {
            fs.unlink('./public/uploads/'+result.file,(err)=>{  //for picture deleting from uploads folder
                
//==================== for comment deleting=========================  
                if(!result.comments.length<1){
                    result.comments.forEach(comment => {
                        comment.remove()
                    });
                }
                result.remove().then(removedpost=>{
                    req.flash('successMessage', `<< Post deleted succesfully >>`)
                    res.redirect('/admin/posts/allpost')
                })
            })
        })
})


module.exports = router