const express = require('express');
const router = express.Router()
const Post = require('..//../models/posts');
const Comment = require('../../models/comments')


router.all('/*', (req, res, next) => {
    req.app.locals.layout = 'admin'
    next()
})


router.get('/', (req, res) => {
    Comment.find({}).sort({date:-1}).populate('user')
        .then(comments => {
            res.render('admin/comment', { comments: comments });
        })

});


router.post('/', (req, res) => {
    Post.findOne({ _id: req.body.id }).then(post => {
        const newComment = new Comment({
            user: req.user.id,
            body: req.body.body
        })

        post.comments.push(newComment)
        post.save().then(postsaved => {
            newComment.save().then(savedcomment => {
                // res.redirect('/post/postID');
                res.redirect(`/post/${post.id}`);
            })
        })
    })
})


router.delete('/:id', (req, res) => {
    Comment.findByIdAndDelete({ _id: req.params.id }).then(data => {
        Post.findOneAndUpdate({ comments: req.params.id }, { $pull: { comments: req.params.id } }, (err, data) => { //this line for deleting comments objectID from db
            if (err) return err
            res.redirect('/admin/comments')
        })
    })
});


router.post('/approvecomment', (req, res) => {

    Comment.findByIdAndUpdate(req.body.id, { $set: { approveComment: req.body.approveComment } }, (err, result) => {
        if (err) return err
        res.send(result)
    })
});

module.exports = router