const express = require('express')
const router = express.Router()
const Post = require('../../models/posts')
const category = require('../../models/category')
const { isEmpty } = require('../../helpers/upload-helper')
const { userAuthenticated, adminAuthenticated } = require('../../helpers/userAuthenticated')
const fs = require('fs');

// admin pannel er jonno alada html veiw tar jonno amra akhane
//default layout k overwrite koresi
router.all('/*', (req, res, next) => {
    req.app.locals.layout = 'home'
    next()
})


// viewing all post using this route
router.get('/', (req, res) => {
    Post.find({}).sort({date:-1}).populate('category').populate('user')
    .then(posts => {   // ekhane category shema theke object pabar jonno populate use kora hoye se ja name and id return kore
        res.setHeader('Content-Type', 'text/html');
        res.render('admin/posts/index', { posts: posts})
    })
})

// for getting post writting page 
router.get('/create', (req, res) => {
    category.find({}).then(category => {
        res.render('admin/posts/create', { category: category })
    })
})


// all information for post and saved to database
router.post('/create', (req, res) => {

    let errors = []

    if (!req.body.title) {
        errors.push({ message: 'Please give a title' })
    }
    if (!req.body.body) {
        errors.push({ message: 'This field can not be empty' })
    }

    if (errors.length > 0) {
        res.render('admin/posts/create', {
            errors: errors
        })
    }
    else {
        let filename = ""
        if (!isEmpty(req.files)) {
            //==========file send to uploads folder
            let file = req.files.file
            filename = Date.now() + '-' + file.name
            file.mv('./public/uploads/' + filename, (err) => {
                if (err) throw err
            })
        }

        //================================================
        let allowComments = true
        if (req.body.allowComments) {
            allowComments = true
        }
        else {
            allowComments = false
        }


        const newPost = new Post({
            title: req.body.title,
            status: req.body.status,
            allowComments: allowComments,
            body: req.body.body,
            file: filename,
            category: req.body.category,
            user : req.user.id
           
        })

        newPost.save().then(datasaved => {
            req.flash('successMessage', `<< ${datasaved.title} saved succesfully >>`)
            res.redirect('/user/allpost')
            console.log(datasaved)

        }).catch(err => console.log('Error!!! post save failed'))
    }

})


// getting page for editing 
router.get('/edit/:id', (req, res) => {
    Post.findOne({ _id: req.params.id }).then(post => {
        category.find({}).then(category => {
            res.render('admin/posts/edit', { post: post, category: category })
        })

    })
})

//for posting edited posts 
router.put('/edit/:id', (req, res) => {

    Post.findOne({ _id: req.params.id }).then(Post => {

        if (req.body.allowComments) {
            allowComments = true
        }
        else {
            allowComments = false
        }

        Post.title = req.body.title
        Post.status = req.body.status
        Post.allowComments = allowComments
        Post.body = req.body.body
        Post.category = req.body.category
        Post.user = req.user.id
       

        if (!isEmpty(req.files)) {
            //==========file send to uploads folder
            let file = req.files.file
            filename = Date.now() + '-' + file.name
            Post.file = filename
            file.mv('./public/uploads/' + filename, (err) => {
                if (err) throw err
            })
        }


        Post.save().then(datasaved => {
            req.flash('successMessage', `<< ${datasaved.title} edited succesfully >>`)
            res.redirect('/user/allpost')
            console.log(datasaved)
        }).catch(err => console.log('Error!!! post save failed'))
    })
})


// for deleting post 
router.delete('/:id', (req, res) => {
    Post.findOne({ _id: req.params.id })
        .populate('comments')
        .then(result => {
            fs.unlink('./public/uploads/'+result.file,(err)=>{  //for picture deleting from uploads folder
                
//==================== for comment deleting=========================  
                if(!result.comments.length<1){
                    result.comments.forEach(comment => {
                        comment.remove()
                    });
                }
                result.remove().then(removedpost=>{
                    req.flash('successMessage', `<< Post deleted succesfully >>`)
                    res.redirect('/user/allpost')
                })
            })
        })
})



module.exports = router

